<?php

  /**
   *
   */
  class Pruebas extends CI_Controller

  {

    function __construct(){
      parent::__construct();
    }

    public function index(){
      $this->load->view('header');
      $this->load->view('pruebas/index');
      $this->load->view('footer');
    }

    public function nuevo(){
      $this->load->view('header');
      $this->load->view('pruebas/nuevo');
      $this->load->view('footer');
    }

    public function luna(){
      $this->load->view('header');
      $this->load->view('pruebas/luna');
      $this->load->view('footer');
    }

    public function sol(){
      $this->load->view('header');
      $this->load->view('pruebas/sol');
      $this->load->view('footer');
    }
  }



 ?>
